# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from registration.users import UserModel
import string, random

def run():
    for i in range(50):
        rand_num = random.randint(3,5)
        rand_str0 = string.join(random.sample(['小红花','小明','宁强','宁羌','小红帽','红顶子','菊花','星宇','星星','c罗','梅西','后马路杀手','汉中２哥','羌族','小丁丁','巴山','蓝翔师兄','酸菜','小妹妹','明镜','半个太阳','关中大侠','八戒','宁强万人迷'], 1)).replace(' ','')
        rand_str1 = string.join(random.sample(['x','w','v','u','t','s','r','q','p','o','n','m','l','k','j','i','h','g','f','e','d','c','b','a'], rand_num)).replace(' ','')
        username = rand_str0 + rand_str1
        email = rand_str1 + "@qq.com"
        user = UserModel().objects.create_user(username, email, "88176824")
        user.profile.signdate = "20160101"
        user.is_active = True
        user.save()

from django.contrib import admin
from bootcamp.auth import models

class ProfileAdmin(admin.ModelAdmin):
    pass

admin.site.register(models.Profile, ProfileAdmin)

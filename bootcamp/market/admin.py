from django.contrib import admin
from bootcamp.market import models

class OrderAdmin(admin.ModelAdmin):
    pass

class ShopAdmin(admin.ModelAdmin):
    pass

class GoodsAdmin(admin.ModelAdmin):
    pass

admin.site.register(models.Order, OrderAdmin)
admin.site.register(models.Shop, ShopAdmin)
admin.site.register(models.Goods, GoodsAdmin)

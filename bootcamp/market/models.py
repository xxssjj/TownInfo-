from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from bootcamp.activities.models import Activity
from django.utils.html import escape
import bleach

class Shop(models.Model):
    name = models.CharField(max_length=50)
    addr = models.CharField(max_length=255)
    user = models.ForeignKey(User)
    def __unicode__(self):
        return self.name

class Order(models.Model):
    user = models.ForeignKey(User)
    shop = models.ForeignKey(Shop)
    date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(default=0)
    content = models.CharField(max_length=150)

    class Meta:
        verbose_name = 'orders'
        verbose_name_plural = 'orders'
        ordering = ('-date', 'status')

    def __unicode__(self):
        return str(self.id)

class Goods(models.Model):
    order = models.ForeignKey(Order)
    name = models.CharField(max_length=50)
    shop = models.ForeignKey(Shop)
    def __unicode__(self):
        return self.name

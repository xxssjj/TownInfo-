from django.conf.urls import patterns, include, url

urlpatterns = patterns('bootcamp.market.views',
    url(r'^list_orders/$', 'list_orders', name='list_orders'),
    url(r'^add_order/$', 'add_order', name='add_order'),
    url(r'^delete_order/$', 'delete_order', name='delete_order'),
)

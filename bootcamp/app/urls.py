from django.conf.urls import patterns, include, url

urlpatterns = patterns('bootcamp.app.views',
    url(r'^login/$', 'login', name='login'),
    url(r'^reg/$', 'reg', name='reg'),
    url(r'^get_state/$', 'get_state', name='get_state'),
    url(r'^set_state/$', 'set_state', name='set_state'),
    url(r'^get_feeds/$', 'get_feeds', name='get_feeds'),
    url(r'^checkupdate/$', 'checkupdate', name='checkupdate'),
    url(r'^post/$', 'post', name='post'),
    url(r'^load/$', 'load', name='load'),
    url(r'^load_new/$', 'load_new', name='load_new'),
    url(r'^comment/$', 'comment', name='comment'),
    url(r'^track_comments/$', 'track_comments', name='track_comments'),
)

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'bootcamp.core.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^logout', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^settings/$', 'bootcamp.core.views.settings', name='settings'),
    url(r'^settings/picture/$', 'bootcamp.core.views.picture', name='picture'),
    url(r'^settings/upload_picture/$', 'bootcamp.core.views.upload_picture', name='upload_picture'),
    url(r'^settings/save_uploaded_picture/$', 'bootcamp.core.views.save_uploaded_picture', name='save_uploaded_picture'),
    url(r'^settings/password/$', 'bootcamp.core.views.password', name='password'),
    url(r'^about/$', 'bootcamp.core.views.about', name='about'),
    url(r'^feeds/', include('bootcamp.feeds.urls')),
    url(r'^app/', include('bootcamp.app.urls')),
    url(r'^market/', include('bootcamp.market.urls')),
    url(r'^questions/', include('bootcamp.questions.urls')),
    url(r'^articles/', include('bootcamp.articles.urls')),
    url(r'^messages/', include('bootcamp.messages.urls')),
    url(r'^notifications/$', 'bootcamp.activities.views.notifications', name='notifications'),
    url(r'^appnote/$', 'bootcamp.activities.views.appnote', name='appnote'),
    url(r'^del_note/$', 'bootcamp.activities.views.del_note', name='del_note'),
    url(r'^notifications/last/$', 'bootcamp.activities.views.last_notifications', name='last_notifications'),
    url(r'^notifications/check/$', 'bootcamp.activities.views.check_notifications', name='check_notifications'),
    url(r'^search/$', 'bootcamp.search.views.search', name='search'),
    url(r'^(?P<username>[^/]+)/$', 'bootcamp.core.views.profile', name='profile'),
    url(r'^i18n/', include('django.conf.urls.i18n', namespace='i18n')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

import logging
import logging.handlers
import os

make_dirs = [
    "/var/log/httpd/",
]
for directory in make_dirs:
    if not os.path.isdir(directory):
        os.makedirs(directory)

LOGGING_LEVEL = {"debug": logging.DEBUG,
                 "info": logging.INFO,
                 "warning": logging.WARNING,
                 "error": logging.ERROR,
                 "critical": logging.CRITICAL}

LOG_LEVEL = LOGGING_LEVEL.get("debug")
LOG_DIR = "/var/log/httpd/"
LOG_PATH = "/var/log/httpd/nqzx.log"
FORMATTER = logging.Formatter('%(levelno)s - %(asctime)s - %(name)s - %(levelname)s : \
        %(message)s')

h = logging.handlers.RotatingFileHandler(LOG_PATH, 'a', 10000000, 1000)
h.setFormatter(FORMATTER)
log = logging.getLogger('NQZX')
log.setLevel(LOG_LEVEL)
log.addHandler(h)

from django.contrib import admin
from bootcamp.feeds import models

class FeedAdmin(admin.ModelAdmin):
    pass

admin.site.register(models.Feed, FeedAdmin)
